package utfpr.ct.dainf.if62c.pratica;

import static java.lang.Double.NaN;
import static javax.management.Query.eq;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
/*import Equacao2Grau;*/
public class Pratica52 {
public static void main(String[] args) {
    
    Equacao2Grau eq = new Equacao2Grau(0,4,8);
    
    try {
         System.out.println("A raiz 01 é: " + eq.getRaiz1());
    } catch (Exception e) {
        System.out.println("Coeficiente a não pode ser zero");
                }
    try {
    System.out.println("A raiz 02 é: " + eq.getRaiz2());
    } catch (Exception e) {
         System.out.println("Equação não tem solução real");
    }
   
    }
}

  
