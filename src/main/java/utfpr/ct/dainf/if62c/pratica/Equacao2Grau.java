/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Danielli Batistella
 */
public class Equacao2Grau {
    double a;
    double b;
    double c;
    
    public Equacao2Grau(double A, double B, double C)
    {
        a=A;
        b=B;
        c=C;         
    }
    
    public double getRaiz1()
    {double Raiz1=(b-(Math.sqrt(b*b-4*a*c)))/0;
    return Raiz1;
       
    }

    
    public double getRaiz2()
    {double Raiz2=(-b-(Math.sqrt(b*b-4*a*c)))/0;
    return Raiz2;    
    } 
    public double getA(){
    return a;
}
    public double getB(){
     return b;
    }
    public double getC(){
      return c;
    }
    
    public double setA() {
        return a;
    }
     public double setB() {
        return b;
     }
        public double setC() {
        return c;
        }
}
